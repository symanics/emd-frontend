function offCanvas() {
    /**
     * Other classed used from original bootstrap4
     */
    var mainNavContainer = '.navbar.navbar-light.bg-faded.main';
    var mainNavClass = '.nav.navbar-nav';

    var metaNavContainer = '.navbar.navbar-light.bg-faded.meta';
    var metaNavClass = '.nav.navbar-nav';

    var brandName = $(mainNavContainer).find('.navbar-brand').text();

    $('body').append('<div id="offCanvasFade"></div>');
    $('body').append('<aside id="offCanvas"><div class="offCanvas-header"><h2>' + brandName + '</h2><span class="close"><i class="fa fa-close"></i></span></div><nav><div class="list-group"></div></nav></aside>');

    /**
     * Build Main-Navigation recursive
     */
    if($(mainNavClass)[0]) {
        $(mainNavContainer).find(mainNavClass).find('.nav-item').each(function(i) {
            var parentName = $(this).find('.nav-link').first().text();
            var parentAlias = $(this).find('.nav-link').first().attr('href').replace("'", "");
            var parentId = 'parent_' + randomNumber(1, 999999999);
            var active = '';

            if($(this).hasClass('active')) {
                active = ' active';
            }

            $('#offCanvas .list-group').append('<a class="list-group-item' + active + '" data-parent="' + parentId + '" href="' + parentAlias + '">' + parentName + '</a>');

            offCanvasLayer($(this), parentId, 1);
        })
    }

    /**
     * Build Meta-Navigation
     */
    if($(metaNavClass)[0]) {

    }

    offCanvasLayerToggle();
    offCanvasToggle();
}

function offCanvasLayer(obj, parent, level) {
    if($(obj[0]).find('.dropdown-menu')[0]) {
        var layerId = 'layer_' + randomNumber(1, 999999999);
        $('a[data-parent="' + parent + '"]').append('<span class="openOffCanvasLayer" data-layer="' + layerId + '"><i class="fa fa-chevron-right"></i></span>');
        $('body').append('<div id="' + layerId + '" data-childOf="' + parent + '" class="offCanvasLayer level_' + level + '"><div class="offCanvas-header"><h2 class="offCanvasLayerToggle"><i class="fa fa-chevron-left"></i> Zurück</h2><span class="close"><i class="fa fa-close"></i></span></div><nav><ul class="list-group"></ul></nav></div>')

        $(obj).find('.dropdown-item').each(function(i) {
            var parentName = $(this).text();
            var parentAlias = $(this).attr('href').replace("'", "");
            var parentId = 'parent_' + randomNumber(1, 999999999);
            var active = '';

            if($(this).hasClass('active')) {
                active = ' active';
            }

            $('#'+ layerId + ' .list-group').append('<a class="list-group-item'+ active + '" data-parent="' + parentId + '" href="' + parentAlias + '">' + parentName + '</a>');
        });
    }
}

function offCanvasToggle() {
    if($('.offCanvasToggle')[0] || $('#offCanvas .close')[0]) {
        $('.offCanvasToggle, #offCanvas .close, .offCanvasLayer .close').on('click', function(e) {
            e.preventDefault();

            $('.offCanvasLayer').each(function() {
               $(this).removeClass('active');
            });

            $('#offCanvas').toggleClass('active');
            $('#offCanvasFade').toggleClass('active');
            $('body').toggleClass('offCanvas-active');
        })
    }
}

function offCanvasLayerToggle() {
    if($('.openOffCanvasLayer')[0] || $('.offCanvasLayerToggle')[0]) {
        $('body').on('click', '.openOffCanvasLayer, .offCanvasLayerToggle', function(e) { console.log('toggle');
            e.preventDefault();
            var layer = $(this).attr('data-layer');
            if(!layer) {
                layer = $(this).parents('.offCanvasLayer').attr('id');
            }

            console.log(layer);

            $('#' + layer).toggleClass('active');
        })
    }
}

function randomNumber(bottom, top) {
    return Math.floor( Math.random() * ( 1 + top - bottom ) ) + bottom;
}